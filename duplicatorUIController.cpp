#include <filesystem>
#include <sstream>
#include <QtWidgets/QMessagebox>
#include "duplicatorUIController.h"
#include "qdialog.h"
#include "duplicateFinder.h"


void duplicatorUIController::handle_start_botton_click_event() {
	duplicateFinder finder = duplicateFinder();
	std::string newText = "Duplicate files in " + finder.getOutFileName();
	start_button->setText(QString::fromStdString(newText));

	finder.findDuplicates(ui.startingPathLE->text().toStdString()); 
	finder.printDuplicates();
}

void duplicatorUIController::handle_menu_about_click_event() {
	QMessageBox *box = new QMessageBox(this);
	box->setIcon(QMessageBox::Information);
	box->setWindowTitle("Manual");
	const auto MESSAGE = QWidget::tr("<h1>Application manual</h1>" \
		"<p>Duplicator finds duplicate file on your filesystem." \
		"<p>Press start button to proceed and find those files.");
	box->setText(MESSAGE);
	box->setInformativeText("beta version");
	QPushButton *btnCancel = box->addButton("Cancel", QMessageBox::RejectRole);
	box->setAttribute(Qt::WA_DeleteOnClose); 
	box->setModal(false);
	box->show();
}

duplicatorUIController::duplicatorUIController(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	start_button = ui.start_button;

	connect(start_button, SIGNAL(clicked()), this, SLOT(handle_start_botton_click_event()));
	connect(ui.actionAbout, SIGNAL(triggered()), this, SLOT(handle_menu_about_click_event()));
}
