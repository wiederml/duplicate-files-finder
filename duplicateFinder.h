#pragma once
#include <string>
#include <forward_list>
#include <unordered_map>
#include <vector>
#include <fstream>

class duplicateFinder
{
	std::string outfileName;
	//unordered map with vector of files of the same size
	std::unordered_map<uintmax_t, std::vector<std::string>> sizeDuplicates;
	std::ofstream outfile;
	public:
		duplicateFinder();
		void findDuplicates(std::string path);
		void printDuplicates();
		inline std::string getOutFileName() { return outfileName; };
};

