#pragma once

#include <QtWidgets/QMainWindow>
#include <QtCore/QObject>
#include "ui_duplicator.h"

class duplicatorUIController : public QMainWindow
{
	Q_OBJECT
public:
	explicit duplicatorUIController(QWidget *parent = 0);
private slots:
	void handle_start_botton_click_event();
	void handle_menu_about_click_event();
private:
	QPushButton *start_button;
	Ui_duplicatorClass ui;
	bool clicked;
};


