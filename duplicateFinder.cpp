#include "duplicateFinder.h"
#include <filesystem>
#include <iostream>

duplicateFinder::duplicateFinder()
{
	outfileName = "duplicatefiles.txt";
}

void duplicateFinder::findDuplicates(std::string path) 
{
	for (const auto& dir : std::filesystem::recursive_directory_iterator(path))
	{
		if (!std::filesystem::is_directory(dir))
		{
			auto key = (std::filesystem::file_size(dir));
			sizeDuplicates[key].push_back(dir.path().string());
		}
	}
	
}

void duplicateFinder::printDuplicates() 
{
	outfile.open(outfileName);
	outfile << "Duplicate files per row!" << std::endl;
	for (auto& it : sizeDuplicates) {
		if (it.second.size() > 1)
		{
			for (auto& vecIt : it.second)
			{
				outfile << " " << vecIt;
			}
			outfile << std::endl << std::endl;
		}
	}
	outfile.close();
}
